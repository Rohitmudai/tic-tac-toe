package com.tictactoe;

import java.util.Scanner;

public class Driver {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println(">>>>>> TIC-TAC-TOE <<<<<<");

        Player currentTurn = Player.X;

        while (true){

            //display board

            //display turn and take choice
            int index = takeUserInput(currentTurn);

            //setX or setO

            //flip turn
            currentTurn = currentTurn == Player.X ? Player.O : Player.X;
        }
    }

    static int takeUserInput(Player obj) {
        int choice;
        if (obj.equals(Player.X)) {
            System.out.println("Player 1 Turn(" + obj.name() + ")");
        } else {
            System.out.println("Player 2 Turn(" + obj.name() + ")");
        }
        System.out.println("Enter your choice(1-9) :");
        choice = sc.nextInt();
        return choice;
    }


}
